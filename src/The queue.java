// Queue.java
// Demonstrates a queue
// To run this program: C>java QueueApp
////////////////////////////////////////////////////////////////
class Queue {
    private int maxSize;
    private long[] queArray;
    private int front;
    private int rear;
    private int nItems;

    public Queue(int s) // Constructor
    {
        maxSize = s;
        queArray = new long[maxSize];
        front = 0;
        rear = -1;
        nItems = 0;
    }

    public void insert(long j) // Put an item at the rear of the queue
    {
        if (rear == maxSize - 1) // Deal with wraparound
            rear = -1;
        queArray[++rear] = j; // Increment rear and insert
        nItems++; // One more item
    }

    public long remove() // Take an item from the front of the queue
    {
        long temp = queArray[front++]; // Get value and increment front
        if (front == maxSize) // Deal with wraparound
            front = 0;
        nItems--; // One less item
        return temp;
    }

    public long peekFront() // Peek at the front of the queue
    {
        return queArray[front];
    }

    public boolean isEmpty() // True if the queue is empty
    {
        return (nItems == 0);
    }

    public boolean isFull() // True if the queue is full
    {
        return (nItems == maxSize);
    }

    public int size() // Number of items in the queue
    {
        return nItems;
    }
} // end class Queue

////////////////////////////////////////////////////////////////
class QueueApp {
    public static void main(String[] args) {
        Queue theQueue = new Queue(5); // Queue holds 5 items
        theQueue.insert(10); // Insert 4 items
        theQueue.insert(20);
        theQueue.insert(30);
        theQueue.insert(40);
        theQueue.remove(); // Remove 3 items (10, 20, 30)
        theQueue.remove();
        theQueue.remove();
        theQueue.insert(50); // Insert 4 more items (wraps around)
        theQueue.insert(60);
        theQueue.insert(70);
        theQueue.insert(80);
        while (!theQueue.isEmpty()) // Remove and display all items (40, 50, 60, 70, 80)
        {
            long n = theQueue.remove();
            System.out.print(n);
            System.out.print(" ");
        }
        System.out.println("");
    } // end main()
} // end class QueueApp
