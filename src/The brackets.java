// Brackets.java
// Stacks used to check matching brackets
// To run this program: C>java BracketsApp
import java.io.*; // for I/O

class StackX {
    private int maxSize;
    private char[] stackArray;
    private int top;

    public StackX(int s) // constructor
    {
        maxSize = s;
        stackArray = new char[maxSize];
        top = -1;
    }

    public void push(char j) // put an item on top of the stack
    {
        stackArray[++top] = j;
    }

    public char pop() // take an item from the top of the stack
    {
        return stackArray[top--];
    }

    public char peek() // peek at the top of the stack
    {
        return stackArray[top];
    }

    public boolean isEmpty() // true if the stack is empty
    {
        return (top == -1);
    }
} // end class StackX

class BracketChecker {
    private String input; // input string

    public BracketChecker(String in) // constructor
    {
        input = in;
    }

    public void check() {
        int stackSize = input.length(); // get max stack size
        StackX theStack = new StackX(stackSize); // make a stack
        for (int j = 0; j < input.length(); j++) // get chars in turn
        {
            char ch = input.charAt(j); // get char
            switch (ch) {
                case '{': // opening symbols
                case '[':
                case '(':
                    theStack.push(ch); // push them
                    break;
                case '}': // closing symbols
                case ']':
                case ')':
                    if (!theStack.isEmpty()) // if the stack is not empty,
                    {
                        char chx = theStack.pop(); // pop and check
                        if ((ch == '}' && chx != '{') ||
                            (ch == ']' && chx != '[') ||
                            (ch == ')' && chx != '('))
                            System.out.println("Error: " + ch + " at " + j);
                    } else // prematurely empty
                        System.out.println("Error: " + ch + " at " + j);
                    break;
                default: // no action on other characters
                    break;
            } // end switch
        } // end for
        // at this point, all characters have been processed
        if (!theStack.isEmpty())
            System.out.println("Error: missing right delimiter");
    } // end check()
} // end class BracketChecker

class BracketsApp {
    public static void main(String[] args) throws IOException {
        String input;
        while (true) {
            System.out.print("Enter a string containing delimiters: ");
            System.out.flush();
            input = getString(); // read a string from the keyboard
            if (input.equals("")) // quit if [Enter]
                break;
            // make a BracketChecker
            BracketChecker theChecker = new BracketChecker(input);
            theChecker.check(); // check brackets
        } // end while
    } // end main()

    public static String getString() throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        String s = br.readLine();
        return s;
    }
} // end class BracketsApp

