import java.io.IOException;

class PriorityQ {
    private int maxSize;
    private long[] queArray;
    private int nItems;

    public PriorityQ(int s) // Constructor
    {
        maxSize = s;
        queArray = new long[maxSize];
        nItems = 0;
    }

    public void insert(long item) // Insert item
    {
        int j;
        if (nItems == 0) // If no items, insert at 0
            queArray[nItems++] = item;
        else // If items,
        {
            for (j = nItems - 1; j >= 0; j--) // Start at the end
            {
                if (item > queArray[j]) // If new item is larger,
                    queArray[j + 1] = queArray[j]; // Shift upward
                else // If smaller, done shifting
                    break;
            }
            queArray[j + 1] = item; // Insert the item
            nItems++;
        }
    }

    public long remove() // Remove minimum item
    {
        return queArray[--nItems];
    }

    public long peekMin() // Peek at minimum item
    {
        return queArray[nItems - 1];
    }

    public boolean isEmpty() // True if the queue is empty
    {
        return (nItems == 0);
    }

    public boolean isFull() // True if the queue is full
    {
        return (nItems == maxSize);
    }
} // end class PriorityQ

class PriorityQApp {
    public static void main(String[] args) throws IOException {
        PriorityQ thePQ = new PriorityQ(5);
        thePQ.insert(30);
        thePQ.insert(50);
        thePQ.insert(10);
        thePQ.insert(40);
        thePQ.insert(20);
        while (!thePQ.isEmpty()) {
            long item = thePQ.remove();
            System.out.print(item + " "); // 10, 20, 30, 40, 50
        }
        System.out.println("");
    } // end main()
}
