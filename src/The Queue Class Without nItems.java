class Queue {
    private int maxSize;
    private long[] queArray;
    private int front;
    private int rear;

    public Queue(int s) // Constructor
    {
        maxSize = s + 1; // Array is 1 cell larger than requested
        queArray = new long[maxSize];
        front = 0;
        rear = -1;
    }

    public void insert(long j) // Put item at rear of the queue
    {
        if (rear == maxSize - 1)
            rear = -1;
        queArray[++rear] = j;
    }

    public long remove() // Take item from the front of the queue
    {
        long temp = queArray[front++];
        if (front == maxSize)
            front = 0;
        return temp;
    }

    public long peek() // Peek at the front of the queue
    {
        return queArray[front];
    }

    public boolean isEmpty() // True if the queue is empty
    {
        return (rear + 1 == front || (front + maxSize - 1 == rear));
    }

    public boolean isFull() // True if the queue is full
    {
        return (rear + 2 == front || (front + maxSize - 2 == rear));
    }

    public int size() // (Assumes queue is not empty)
    {
        if (rear >= front) // Contiguous sequence
            return rear - front + 1;
        else // Broken sequence
            return (maxSize - front) + (rear + 1);
    }
} // end class Queue
